package AMHoneywellPlugin;
// import javax.swing.text.View;

import com.honeywell.aidc.AidcManager;
import com.honeywell.aidc.AidcManager.CreatedCallback;
import com.honeywell.aidc.BarcodeFailureEvent;
import com.honeywell.aidc.BarcodeReadEvent;
import com.honeywell.aidc.BarcodeReader;
import com.honeywell.aidc.ScannerUnavailableException;
import com.honeywell.aidc.ScannerNotClaimedException;

// v 0.0.1
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

// import androidx.appcompat.app.AppCompatActivity;

public class AMHoneywellPlugin extends CordovaPlugin implements BarcodeReader.BarcodeListener {

    public static final String LOG_TAG = "ampluginlog";

    // private static final String TAG = "HoneywellScanner";
    private static BarcodeReader barcodeReader;
    private AidcManager manager;
    private CallbackContext callbackContext;
    private Context context;

    private static final String TAG = "IntentApiSample";

    private static final String EXTRA_CONTROL = "com.honeywell.aidc.action.ACTION_CONTROL_SCANNER";
    private static final String EXTRA_SCAN = "com.honeywell.aidc.extra.EXTRA_SCAN";

    public static final String ACTION_BARCODE_DATA = "com.honeywell.sample.intentapisample.BARCODE";
    private TextView textView;
    int sdkVersion = Build.VERSION.SDK_INT;
    String model = Build.MODEL;

    // public BroadcastReceiver barcodeDataReceiver;

    /**
     * Honeywell DataCollection Intent API
     * Claim scanner
     * Permissions:
     * "com.honeywell.decode.permission.DECODE"
     */
    public static final String ACTION_CLAIM_SCANNER = "com.honeywell.aidc.action.ACTION_CLAIM_SCANNER";

    /**
     * Honeywell DataCollection Intent API
     * Release scanner claim
     * Permissions:
     * "com.honeywell.decode.permission.DECODE"
     */
    public static final String ACTION_RELEASE_SCANNER = "com.honeywell.aidc.action.ACTION_RELEASE_SCANNER";

    /**
     * Honeywell DataCollection Intent API
     * Optional. Sets the scanner to claim. If scanner is not available or if extra
     * is not used,
     * DataCollection will choose an available scanner.
     * Values : String
     * "dcs.scanner.imager" : Uses the internal scanner
     * "dcs.scanner.ring" : Uses the external ring scanner
     */
    public static final String EXTRA_SCANNER = "com.honeywell.aidc.extra.EXTRA_SCANNER";

    /**
     * Honeywell DataCollection Instent API
     * Optional. Sets the profile to use. If profile is not available or if extra is
     * not used,
     * the scanner will use factory default properties (not "DEFAULT" profile
     * properties).
     * Values : String
     */
    public static final String EXTRA_PROFILE = "com.honeywell.aidc.extra.EXTRA_PROFILE";

    /**
     * Honeywell DataCollection Intent API
     * Optional. Overrides the profile properties (non-persistend) until the next
     * scanner claim.
     * Values : Bundle
     */
    public static final String EXTRA_PROPERTIES = "com.honeywell.aidc.extra.EXTRA_PROPERTIES";
    private Handler updateConversationHandler;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        Log.d(LOG_TAG, "START Initialize");
        super.initialize(cordova, webView);

        context = cordova.getActivity().getApplicationContext();
        AidcManager.create(context, new CreatedCallback() {
            @Override
            public void onCreated(AidcManager aidcManager) {
                Log.d(LOG_TAG, "START onCreated of callback");
                manager = aidcManager;
                barcodeReader = manager.createBarcodeReader();
                if (barcodeReader != null) {
                    barcodeReader.addBarcodeListener(AMHoneywellPlugin.this);
                    // try {
                    // barcodeReader.claim();
                    
                    // claimScanner();
                    
                    // } catch (ScannerUnavailableException e) {
                    // e.printStackTrace();
                    // NotifyError("ScannerUnavailableException2");
                    // }
                }
            }
        });
    }

    // @Override
    // protected void onCreate(Bundle savedInstanceState) {
    // super.onCreate(savedInstanceState);
    // setContentView(R.layout.activity_main);
    // textView = findViewById(R.id.textView);
    // textView.setText("sdk version: " + sdkVersion);

    // }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);

        Log.d(LOG_TAG, "START onResume of Plugin");

        // Register receiver so my app can listen for intents which action is
        // ACTION_BARCODE_DATA
        IntentFilter intentFilter = new IntentFilter(ACTION_BARCODE_DATA);
        context.registerReceiver(barcodeDataReceiver, intentFilter);

        // Will setup the new configuration of the scanner.
        claimScanner();
    }

    @Override
    public void onBarcodeEvent(BarcodeReadEvent barcodeReadEvent) {
        Log.d(LOG_TAG, "START onBarcodeEvent of Plugin");
        if (this.callbackContext != null) {
            PluginResult result = new PluginResult(PluginResult.Status.OK, barcodeReadEvent.getBarcodeData());
            result.setKeepCallback(true);
            this.callbackContext.sendPluginResult(result);
        }
    }

    @Override
    public void onFailureEvent(BarcodeFailureEvent barcodeFailureEvent) {
        Log.d(LOG_TAG, "START onFailureEvent of Plugin");
        NotifyError("Scan has failed");
    }

    private void claimScanner() {

        Log.d(LOG_TAG, "START claimScanner of Plugin");

        Bundle properties = new Bundle();

        // When we press the scan button and read a barcode, a new Broadcast intent will
        // be launched by the service
        properties.putBoolean("DPR_DATA_INTENT", true);

        // That intent will have the action "ACTION_BARCODE_DATA"
        // We will capture the intents with that action (every scan event while in the
        // application)
        // in our BroadcastReceiver barcodeDataReceiver.
        properties.putString("DPR_DATA_INTENT_ACTION", ACTION_BARCODE_DATA);
        // properties.putString("TRIGGER_MODE", "continuous");

        Intent intent = new Intent();
        intent.setAction(ACTION_CLAIM_SCANNER);

        /*
         * We use setPackage() in order to send an Explicit Broadcast Intent, since it
         * is a requirement
         * after API Level 26+ (Android 8)
         */
        intent.setPackage("com.intermec.datacollectionservice");

        // We will use the internal scanner
        intent.putExtra(EXTRA_SCANNER, "dcs.scanner.imager");

        /*
         * We are using "MyProfile1", so a profile with this name has to be created in
         * Scanner settings:
         * Android Settings > Honeywell Settings > Scanning > Internal scanner > "+"
         * - If we use "DEFAULT" it will apply the settings from the Default profile in
         * Scanner settings
         * - If not found, it will use Factory default settings.
         */
        intent.putExtra(EXTRA_PROFILE, "DEFAULT");
        intent.putExtra(EXTRA_PROPERTIES, properties);

        context.sendBroadcast(intent);
        // Toast.makeText(this, "Scanner Claimed", Toast.LENGTH_SHORT).show();
    }

    public void triggerScanner(View view) {

        Log.d(LOG_TAG, "START triggerScanner of Plugin");

        context.sendBroadcast(new Intent(EXTRA_CONTROL)
                .setPackage("com.intermec.datacollectionservice")
                .putExtra(EXTRA_SCAN, true));
        // Toast.makeText(this, "Releasing the Scanner", Toast.LENGTH_SHORT).show();
    }

    // private void setText(final String text) {
    // if (textView != null) {
    // runOnUiThread(new Runnable() {
    // @Override
    // public void run() {
    // textView.setText(text);
    // }
    // });
    // }
    // }

    @Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);

        Log.d(LOG_TAG, "START onPause of Plugin");

        context.unregisterReceiver(barcodeDataReceiver);
        releaseScanner();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(LOG_TAG, "START onDestroy of Plugin");

        if (barcodeReader != null) {
            barcodeReader.close();
            barcodeReader = null;
        }

        if (manager != null) {
            manager.close();
        }
    }

    private void releaseScanner() {

        Log.d(LOG_TAG, "START releaseScanner of Plugin");

        Intent intent = new Intent();
        intent.setAction(ACTION_RELEASE_SCANNER);
        context.sendBroadcast(intent);
    }

    private String bytesToHexString(byte[] array) {

        Log.d(LOG_TAG, "START bytesToHexString of Plugin");

        String s = "[]";
        if (array != null) {
            s = "[";
            for (int i = 0; i < array.length; i++) {
                s += "0x" + Integer.toHexString(array[i]) + ", ";
            }
            s = s.substring(0, s.length() - 2) + "]";
        }
        return s;
    }

    private BroadcastReceiver barcodeDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(LOG_TAG, "START onReceive of BroadcastReceiver");

            Toast.makeText(context, "Received the Broadcast Intent", Toast.LENGTH_SHORT).show();
            String action = intent.getAction();
            System.out.println("Action Received: " + action);

            Log.d(LOG_TAG, "Action : " + action);
            if (ACTION_BARCODE_DATA.equals(action)) {
                /*
                 * These extras are available:
                 * "version" (int) = Data Intent Api version
                 * "aimId" (String) = The AIM Identifier
                 * "charset" (String) = The charset used to convert "dataBytes" to "data" string
                 * "codeId" (String) = The Honeywell Symbology Identifier
                 * "data" (String) = The barcode data as a String
                 * "dataBytes" (byte[]) = The barcode data as a byte array
                 * "timestamp" (String) = The barcode timestamp
                 */

                int version = intent.getIntExtra("version", 0);
                if (version >= 1) {
                    String aimId = intent.getStringExtra("aimId");
                    String charset = intent.getStringExtra("charset");
                    String codeId = intent.getStringExtra("codeId");
                    String data = intent.getStringExtra("data");
                    byte[] dataBytes = intent.getByteArrayExtra("dataBytes");
                    String dataBytesStr = bytesToHexString(dataBytes);
                    String timestamp = intent.getStringExtra("timestamp");

                    String text = String.format(
                            "Data:%s\n" +
                                    "Charset:%s\n" +
                                    "Bytes:%s\n" +
                                    "AimId:%s\n" +
                                    "CodeId:%s\n" +
                                    "Timestamp:%s\n",
                            data, charset, dataBytesStr, aimId, codeId, timestamp);
                    Log.d(LOG_TAG, data );
                    Log.d(LOG_TAG, "Received the scanned barcode");

                    if (callbackContext != null) {
                        PluginResult result = new PluginResult(PluginResult.Status.OK, data);
                        result.setKeepCallback(true);
                        callbackContext.sendPluginResult(result);
                    }

                    // setText(text);

                }
            }
        }
    };

    @Override
    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext)
            throws JSONException {

        Log.d(LOG_TAG, "START execute of Plugin");

        // if (action.equals("softwareTriggerStart")) {
        // if (barcodeReader != null) {
        // try {
        // barcodeReader.softwareTrigger(true);
        // } catch (ScannerNotClaimedException e) {
        // e.printStackTrace();
        // NotifyError("ScannerNotClaimedException");
        // } catch (ScannerUnavailableException e) {
        // e.printStackTrace();
        // NotifyError("ScannerUnavailableException");
        // }
        // }
        // } else if (action.equals("softwareTriggerStop")) {
        // if (barcodeReader != null) {
        // try {
        // barcodeReader.softwareTrigger(false);
        // } catch (ScannerNotClaimedException e) {
        // e.printStackTrace();
        // NotifyError("ScannerNotClaimedException");
        // } catch (ScannerUnavailableException e) {
        // e.printStackTrace();
        // NotifyError("ScannerUnavailableException");
        // }
        // }
        // } else
        if (action.equals("listen")) {
            this.callbackContext = callbackContext;

            Log.d(LOG_TAG, "START LISTEN of Plugin");

            // this.barcodeDataReceiver = new BroadcastReceiver() {

            // @Override
            // public void onReceive(Context context, Intent intent) {

            // Log.d(LOG_TAG, "START onReceive of LISTEN");

            // // Toast.makeText(context, "Received the Broadcast Intent",
            // // Toast.LENGTH_SHORT).show();
            // String action = intent.getAction();
            // System.out.println("Action Received: " + action);
            // if (ACTION_BARCODE_DATA.equals(action)) {
            // /*
            // * These extras are available:
            // * "version" (int) = Data Intent Api version
            // * "aimId" (String) = The AIM Identifier
            // * "charset" (String) = The charset used to convert "dataBytes" to "data"
            // string
            // * "codeId" (String) = The Honeywell Symbology Identifier
            // * "data" (String) = The barcode data as a String
            // * "dataBytes" (byte[]) = The barcode data as a byte array
            // * "timestamp" (String) = The barcode timestamp
            // */

            // int version = intent.getIntExtra("version", 0);
            // if (version >= 1) {
            // String aimId = intent.getStringExtra("aimId");
            // String charset = intent.getStringExtra("charset");
            // String codeId = intent.getStringExtra("codeId");
            // String data = intent.getStringExtra("data");
            // byte[] dataBytes = intent.getByteArrayExtra("dataBytes");
            // String dataBytesStr = bytesToHexString(dataBytes);
            // String timestamp = intent.getStringExtra("timestamp");

            // String text = String.format(
            // "Data:%s\n" +
            // "Charset:%s\n" +
            // "Bytes:%s\n" +
            // "AimId:%s\n" +
            // "CodeId:%s\n" +
            // "Timestamp:%s\n",
            // data, charset, dataBytesStr, aimId, codeId, timestamp);
            // Log.d(LOG_TAG, "Received the scanned barcode");
            // // setText(text);

            // if (callbackContext != null) {
            // PluginResult result = new PluginResult(PluginResult.Status.OK, data);
            // result.setKeepCallback(true);
            // callbackContext.sendPluginResult(result);
            // }

            // }
            // }
            // }
            // };

            // Log.d(LOG_TAG, "START NO_RESULT of LISTEN");

            PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT);
            result.setKeepCallback(true);
            this.callbackContext.sendPluginResult(result);
            // if (barcodeReader != null) {

            // try {

            IntentFilter intentFilter = new IntentFilter(ACTION_BARCODE_DATA);
            context.registerReceiver(barcodeDataReceiver, intentFilter);

            claimScanner();
            // barcodeReader.softwareTrigger(false);
            // } catch (ScannerNotClaimedException e) {
            // e.printStackTrace();
            // NotifyError("ScannerNotClaimedException2");
            // } catch (ScannerUnavailableException e) {
            // e.printStackTrace();
            // NotifyError("ScannerUnavailableException2");
            // }
            // }
        }
        // else if (action.equals("claim")) {
        // if (barcodeReader != null) {
        // try {
        // barcodeReader.claim();
        // } catch (ScannerUnavailableException e) {
        // e.printStackTrace();
        // NotifyError("Scanner unavailable");
        // }
        // }
        // if (barcodeReader != null) {
        // try {
        // barcodeReader.softwareTrigger(false);
        // } catch (ScannerNotClaimedException e) {
        // e.printStackTrace();
        // NotifyError("ScannerNotClaimedException2");
        // } catch (ScannerUnavailableException e) {
        // e.printStackTrace();
        // NotifyError("ScannerUnavailableException2");
        // }
        // }
        else if (action.equals("release")) {
            // if (barcodeReader != null) {
            // barcodeReader.release();
            // }
            // if (barcodeReader != null) {
            // try {

            Log.d(LOG_TAG, "START releaseScanner of LISTEN");
            releaseScanner();
            // barcodeReader.softwareTrigger(false);
            // } catch (ScannerNotClaimedException e) {
            // e.printStackTrace();
            // NotifyError("ScannerNotClaimedException2");
            // } catch (ScannerUnavailableException e) {
            // e.printStackTrace();
            // NotifyError("ScannerUnavailableException2");
            // }
            // }
        }
        return true;
    }

    private void NotifyError(String error) {

        Log.d(LOG_TAG, "START NotifyError of plugin");

        if (this.callbackContext != null) {
            PluginResult result = new PluginResult(PluginResult.Status.ERROR, error);
            result.setKeepCallback(true);
            this.callbackContext.sendPluginResult(result);
        }
    }

}
