var execute = require("cordova/exec");

var honeywell = {

    listen: function (res, err) {
        return execute(res, err, 'AMHoneywellPlugin', 'listen', []);
    },

    release: function () {
        return execute(null, null, 'AMHoneywellPlugin', 'release', []);
    },

};

module.exports = honeywell;